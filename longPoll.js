
function LongPoll() {
    var self = this;

    var openRequests = [];

    function work(payload) {
        if (openRequests.length > 0) {
            openRequests.forEach(function (r) {
                var res = r[1];

                res.end(payload);
            });
            openRequests.length = 0;
        }
    }

    self.ping = function (payload) {
        work(payload);
    };

    self.handleRequest = function (req, res) {
        openRequests.push([req, res]);
    };

    self.cleanUp = function() {
        openRequests.forEach(function(r) {
            console.log('Closing connection');
            var res = r[1];
            res.end('');
        });
        openRequests.length = 0;
    }
}

module.exports = LongPoll;
