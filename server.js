
var LongPoll = require('./longPoll.js');
var longPoll = new LongPoll();

var http = require('http');
http.createServer(function (req, res) {
    longPoll.handleRequest(req,res);
}).listen(1337, '127.0.0.1');
console.log('Server running at http://127.0.0.1:1337/');

setInterval(function() {
	longPoll.ping('The time is: ' + Date.now() + '\n');
}, 5000);