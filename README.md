Minimal example for a HTTP long-polling server in Node.js
Sends a ping to clients every 5 seconds.

Run with

	node server.js

Call

	curl http://localhost:1337

or open the URL in your browser.

Note how the connection is kept open until the 5 second interval passes.

